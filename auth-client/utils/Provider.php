<?php


namespace utils;


class provider implements ProviderInterface
{
    private $provider;

    private $appId;
    private $secret;
    private $state;

    private $authorizationUrl;
    private $accessTokenUrl;
    private $userDataUrl;

    public function __construct($json, $provider)
    {
        $this->provider = $provider;
        $this->appId = $json["appId"];
        $this->secret = $json["clientSecret"];
        $this->state = $json["state"];
        $this->authorizationUrl = $json["authorizationUrl"];
        $this->accessTokenUrl = $json["accessTokenUrl"];
        $this->userDataUrl = $json["userDataUrl"];
    }

    public function getProviderName(): string
    {
       return $this->provider;
    }

    public function getAuthorizationUrl(): string
    {
        $param = "client_id=" . $this->appId . "&state=" . $this->state;
        return $this->authorizationUrl . $param;
    }

    public function getAccessTokenUrl(): string
    {
        $param = "client_id=" . $this->appId . "&client_secret=" . $this->secret;
        return $this->accessTokenUrl . $param;
    }

    public function getUsersInfo($token)
    {
        return file_get_contents($this->userDataUrl."&token=".$token);
    }
}