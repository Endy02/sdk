<?php

namespace utils;

interface ProviderInterface
{
    /**
     * Return the provider name
     * @example google, facebook, ...
     */
    public function getProviderName(): string;

    /**
     * Return the authorization endpoint
     * @example http://auth-server/auth
     */
    public function getAuthorizationUrl(): string;

    /**
     * Return the access token endpoint
     * @example http://auth-server/token
     */
    public function getAccessTokenUrl(): string;

    /**
     * Return the user information
     * @param $token
     * @return array
     * ]
     */
    public function getUsersInfo($token);
}